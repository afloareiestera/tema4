function oldPassword() {
    var a = document.getElementById("oldPass");
    if (a.type === "password") {
        a.type = "text";
    } else {
        a.type = "password";
    }
}
function newPassword() {
    var b = document.getElementById("newPass");
    if (b.type === "password") {
        b.type = "text";
    } else {
        b.type = "password";
    }
}
function repeatPassword() {
    var c = document.getElementById("repeatPass");
    if (c.type === "password") {
        c.type = "text";
    } else {
        c.type = "password";
    }
}
window.onload = function () {
    document.getElementById('submit').onclick = function () {
        var pass1 = document.getElementById('newPass').value;
        var pass2 = document.getElementById('repeatPass').value;
        if (pass1 && pass2) {
            if (pass1 == pass2)
                alert('Parola a fost schimbata!');
            else
                alert("Cele doua parole nu corespund!")
        }
    }
}
